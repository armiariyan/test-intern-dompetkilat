# Test Intern DompetKilat

This project contain code for intern test for back end dev at dompet kilat

## Start the docker

1. docker build . -t armiariyan/test-intern
2. docker-compose up --build 
3. access database at http://localhost:30002/
4. make User and Data table in belajar_express database using command at belajar_express.sql
5. try at postman
